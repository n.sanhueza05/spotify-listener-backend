# Creacion de Binario
FROM golang:alpine AS build

RUN apk --no-cache add git

WORKDIR /app

RUN touch .env

ARG CLIENT_ID
ARG CLIENT_SECRET
ARG REFRESH_TOKEN

RUN echo "CLIENT_ID=${CLIENT_ID}" > .env
RUN echo "CLIENT_SECRET=${CLIENT_SECRET}" >> .env
RUN echo "REFRESH_TOKEN=${REFRESH_TOKEN}" >> .env

COPY . .

# Exponer el puerto de la aplicación
EXPOSE 15000


# Compilar la aplicación
RUN go build -o /out/myapp


# Ejecutar la aplicación
CMD ["/out/myapp"]


