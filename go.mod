module spawnbig/spotify-listener-backend

go 1.19

require (
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/joho/godotenv v1.5.1 // indirect
	github.com/rs/cors v1.8.3 // indirect
)
