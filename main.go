package main

import (
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"github.com/rs/cors"
	"log"
	"net/http"
	"spawnbig/spotify-listener-backend/routes"
)

func main() {
	router := mux.NewRouter()
	godotenv.Load()

	router.HandleFunc("/", routes.DefaultResponse).Methods("GET")
	router.HandleFunc("/api/get_top_10", routes.GetGlobalData).Methods("GET")
	router.HandleFunc("/api/get_refresh_token/{code}", routes.GetRefreshToken).Methods("GET")
	router.HandleFunc("/api/get_user_data", routes.GetDataUser).Methods("GET")
	handler := cors.AllowAll().Handler(router)

	log.Println("LISTENING ON PORT 15000")
	err := http.ListenAndServe(":15000", handler)
	if err != nil {
		return
	}
}
