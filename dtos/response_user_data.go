package dtos

type ResponseUserData struct {
	CurrentlyPlaying CurrentlyPlaying `json:"currently_playing"`
	TopArtists       TopArtists       `json:"top_artists"`
}
