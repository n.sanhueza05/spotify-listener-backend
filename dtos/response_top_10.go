package dtos

type ResponseTop10 struct {
	Name  string      `json:"Name"`
	Items []Top10Item `json:"items"`
}

type Top10Item struct {
	Track Top10Track `json:"track"`
}

type Top10Track struct {
	Album Top10Album `json:"album"`
}

type Top10Album struct {
	Name    string        `json:"name"`
	Artists []Top10Artist `json:"artists"`
	Images  []Image       `json:"images"`
}

type Top10Artist struct {
	Name string `json:"name"`
}

type Image struct {
	Height int64  `json:"height"`
	URL    string `json:"url"`
}
