package dtos

type ResponseCurrentPlayingUser struct {
	IsPlaying  bool   `json:"is_playing"`
	Image      string `json:"image"`
	ArtistName string `json:"artist_name"`
	SongName   string `json:"song_name"`
}
