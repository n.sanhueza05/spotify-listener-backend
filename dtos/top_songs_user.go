package dtos

type Top5Songs struct {
	Items []Top5SongsItem `json:"items"`
}

type Top5SongsItem struct {
	Album   Top5Album    `json:"album"`
	Artists []Top5Artist `json:"artists"`
	ID      string       `json:"id"`
	Name    string       `json:"name"`
}

type Top5Album struct {
	Images []Image `json:"images"`
}

type Top5Artist struct {
	ExternalUrls ExternalUrls `json:"external_urls"`
	Href         string       `json:"href"`
	ID           string       `json:"id"`
	Name         string       `json:"name"`
	Type         string       `json:"type"`
	URI          string       `json:"uri"`
}

type ExternalUrls struct {
	Spotify string `json:"spotify"`
}
