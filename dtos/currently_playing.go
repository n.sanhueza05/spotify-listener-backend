package dtos

type CurrentlyPlaying struct {
	Item      CPItem `json:"item"`
	IsPlaying bool   `json:"is_playing"`
}

type CPItem struct {
	Album   CPAlbum    `json:"album"`
	Artists []CPArtist `json:"artists"`
	Name    string     `json:"name"`
}

type CPAlbum struct {
	Images []Image `json:"images"`
	Name   string  `json:"name"`
}

type CPArtist struct {
	Name string `json:"name"`
}
