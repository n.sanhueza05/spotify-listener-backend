package dtos

type TopArtists struct {
	Items []TopArtistItem `json:"items"`
}

type TopArtistItem struct {
	Genres []string `json:"genres"`
	ID     string   `json:"id"`
	Images []Image  `json:"images"`
	Name   string   `json:"name"`
}
