package dtos

type Recommendations struct {
	Tracks []Track `json:"tracks"`
}

type Track struct {
	Album      RecommendationsAlbum    `json:"album"`
	Artists    []RecommendationsArtist `json:"artists"`
	Name       string                  `json:"name"`
	PreviewURL string                  `json:"preview_url"`
}

type RecommendationsAlbum struct {
	Images []Image `json:"images"`
	Name   string  `json:"name"`
}

type RecommendationsArtist struct {
	Name string `json:"name"`
}
