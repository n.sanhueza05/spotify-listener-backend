package dtos

type GenresSpotify struct {
	Genre string `json:"genre"`
	Count int    `json:"count"`
}
