package routes

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"sort"
	"spawnbig/spotify-listener-backend/dtos"
	"strings"
)

const userCurrent = "https://api.spotify.com/v1/me/player/currently-playing"
const userTopArtist = "https://api.spotify.com/v1/me/top/artists?limit=50"
const userTop5Songs = "https://api.spotify.com/v1/me/top/tracks?limit=10"

const availableGenreSeeds = "https://api.spotify.com/v1/recommendations/available-genre-seeds"

func GetDataUser(w http.ResponseWriter, r *http.Request) {
	authHeader := r.Header.Get("Authorization")
	authHeader = strings.TrimPrefix(authHeader, "Bearer ")
	if authHeader == "" {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("Falta bearer Token"))
		return
	}
	token := GetAccessTokenUser(authHeader)
	getCurrent := GetUserCurrent(token)
	topArtists := GetUserTopArtist(token)
	topSongs := GetUserTopSongs(token)

	var resCurrent dtos.ResponseCurrentPlayingUser

	if getCurrent.Item.Name != "" {
		resCurrent = dtos.ResponseCurrentPlayingUser{
			IsPlaying:  getCurrent.IsPlaying,
			Image:      getCurrent.Item.Album.Images[0].URL,
			ArtistName: getCurrent.Item.Artists[0].Name,
			SongName:   getCurrent.Item.Name,
		}
	}

	mapTop10Artists := GetMapTop5Artists(topArtists)
	mapGenres := GetTopGenres(topArtists)

	recommendations := GetUserRecomendations(token, topSongs, mapGenres, topArtists)

	m := map[string]interface{}{
		"current_playing":   resCurrent,
		"top_5_artists":     mapTop10Artists,
		"top_5_genres":      mapGenres,
		"top_5_songs":       topSongs,
		"recommended_songs": recommendations,
	}

	jsonData, err := json.Marshal(m)
	if err != nil {
		// Manejar el error si ocurre
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Write(jsonData)
}

func GetUserRecomendations(token string, songs dtos.Top5Songs, genres []map[string]interface{}, artists dtos.TopArtists) interface{} {
	urlRecommendations := "https://api.spotify.com/v1/recommendations?seed_artists="
	urlRecommendations += artists.Items[0].ID + ","
	urlRecommendations += artists.Items[1].ID
	genresSeed := getSpotifySeedGenres(token, genres)
	urlRecommendations += genresSeed
	urlRecommendations += "&seed_tracks="
	urlRecommendations += songs.Items[0].ID + ","
	urlRecommendations += songs.Items[1].ID
	urlRecommendations += "&limit=5"

	client := &http.Client{}
	req, err := http.NewRequest("GET", urlRecommendations, nil)
	req.Header.Add("Authorization", "Bearer "+token)
	resp, err := client.Do(req)
	if err != nil {
		log.Println("ERROR AL REALIZAR PETICION")
	}
	body, err := io.ReadAll(resp.Body)
	sb := string(body)
	var recomendedSongs dtos.Recommendations
	json.Unmarshal([]byte(sb), &recomendedSongs)

	return recomendedSongs
}

func getSpotifySeedGenres(token string, genres []map[string]interface{}) string {
	var genreCounts []dtos.GenresSpotify

	for _, m := range genres {
		gc := dtos.GenresSpotify{
			Genre: m["genre"].(string),
			Count: m["count"].(int),
		}
		genreCounts = append(genreCounts, gc)
	}

	client := &http.Client{}
	req, err := http.NewRequest("GET", availableGenreSeeds, nil)
	req.Header.Add("Authorization", "Bearer "+token)
	resp, err := client.Do(req)
	if err != nil {
		log.Println("ERROR AL REALIZAR PETICION")
	}
	body, err := io.ReadAll(resp.Body)
	sb := string(body)
	var genresSeed dtos.Genres
	json.Unmarshal([]byte(sb), &genresSeed)

	var commonGenres []string

	for _, genre := range genresSeed.Genres {
		for _, userGenre := range genreCounts {
			if genre == userGenre.Genre {
				commonGenres = append(commonGenres, genre)
				break
			}
		}
		if len(commonGenres) >= 5 {
			break
		}
	}

	if len(commonGenres) == 0 {
		return ""
	}

	urlSeed := "&seed_genres="
	urlSeed += commonGenres[0]
	return urlSeed
}

func GetUserTopSongs(token string) dtos.Top5Songs {
	client := &http.Client{}
	req, err := http.NewRequest("GET", userTop5Songs, nil)
	req.Header.Add("Authorization", "Bearer "+token)
	resp, err := client.Do(req)
	if err != nil {
		log.Println("ERROR AL REALIZAR PETICION")
	}

	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	sb := string(body)

	var topSongs dtos.Top5Songs
	json.Unmarshal([]byte(sb), &topSongs)
	return topSongs
}

func GetMapTop5Artists(artists dtos.TopArtists) []map[string]interface{} {
	var top5Artists []map[string]interface{}

	for i := 0; i < 5; i++ {
		artist := artists.Items[i]
		artistmap := make(map[string]interface{})
		artistmap["name"] = artist.Name
		artistmap["image"] = artist.Images[0].URL
		top5Artists = append(top5Artists, artistmap)
	}
	return top5Artists
}

func GetTopGenres(artists dtos.TopArtists) []map[string]interface{} {
	genreCount := make(map[string]int)
	for _, artist := range artists.Items {
		for _, genre := range artist.Genres {
			genreCount[genre]++
		}
	}

	genreList := make([]map[string]interface{}, 0)
	for genre, count := range genreCount {
		genreMap := make(map[string]interface{})
		genreMap["genre"] = genre
		genreMap["count"] = count
		genreList = append(genreList, genreMap)
	}

	// Ordenar los géneros por número de apariciones
	sort.Slice(genreList, func(i, j int) bool {
		countI := genreList[i]["count"].(int)
		countJ := genreList[j]["count"].(int)
		return countI > countJ
	})

	// Devolver solo los primeros 5 géneros
	topGenres := genreList[:5]

	return topGenres
}

func GetUserTopArtist(token string) dtos.TopArtists {
	client := &http.Client{}
	req, err := http.NewRequest("GET", userTopArtist, nil)
	req.Header.Add("Authorization", "Bearer "+token)
	resp, err := client.Do(req)
	if err != nil {
		log.Println("ERROR AL REALIZAR PETICION")
	}

	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	sb := string(body)

	var topArtist dtos.TopArtists
	json.Unmarshal([]byte(sb), &topArtist)
	return topArtist
}

func GetUserCurrent(token string) dtos.CurrentlyPlaying {
	client := &http.Client{}
	req, err := http.NewRequest("GET", userCurrent, nil)
	req.Header.Add("Authorization", "Bearer "+token)
	resp, err := client.Do(req)
	if err != nil {
		log.Println("ERROR AL REALIZAR PETICION")
	}

	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)

	if resp.StatusCode == 204 {
		return dtos.CurrentlyPlaying{}
	}

	sb := string(body)
	var currPlaying dtos.CurrentlyPlaying
	json.Unmarshal([]byte(sb), &currPlaying)
	return currPlaying
}

func GetAccessTokenUser(authHeader string) string {
	refreshToken := authHeader
	form := url.Values{}
	form.Add("grant_type", "refresh_token")
	form.Add("refresh_token", refreshToken)

	client := &http.Client{}
	req, err := http.NewRequest("POST", urlAuth, strings.NewReader(form.Encode()))
	if err != nil {
		log.Println("ERROR AL CREAR OBJETO CLIENTE")
	}
	auth := fmt.Sprintf("%s:%s", os.Getenv("CLIENT_ID"), os.Getenv("CLIENT_SECRET"))
	base64Auth := base64.StdEncoding.EncodeToString([]byte(auth))
	basicAuth := fmt.Sprintf("%s %s", "Basic", base64Auth)

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Authorization", basicAuth)
	resp, err := client.Do(req)
	if err != nil {
		log.Println("ERROR AL REALIZAR PETICION")
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	sb := string(body)

	var resTop10 dtos.ResponseAccessToken
	json.Unmarshal([]byte(sb), &resTop10)
	return resTop10.AccesToken
}
