package routes

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"spawnbig/spotify-listener-backend/dtos"
	"strings"
)

const urlRedirect = "https://spotifylistener.nsanhueza.me/auth"

func GetRefreshToken(w http.ResponseWriter, r *http.Request) {
	client := &http.Client{}

	vars := mux.Vars(r)
	code := vars["code"]
	log.Println(code)
	form := url.Values{}
	form.Add("grant_type", "authorization_code")
	form.Add("code", code)
	form.Add("redirect_uri", urlRedirect)

	req, err := http.NewRequest("POST", urlAuth, strings.NewReader(form.Encode()))
	if err != nil {
		log.Println("ERROR AL CREAR OBJETO CLIENTE")
	}
	auth := fmt.Sprintf("%s:%s", os.Getenv("CLIENT_ID"), os.Getenv("CLIENT_SECRET"))
	base64Auth := base64.StdEncoding.EncodeToString([]byte(auth))
	basicAuth := fmt.Sprintf("%s %s", "Basic", base64Auth)

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Authorization", basicAuth)

	resp, err := client.Do(req)
	if err != nil {
		log.Println("ERROR AL REALIZAR PETICION")
	}
	defer resp.Body.Close()

	statusCode := resp.StatusCode

	body, err := io.ReadAll(resp.Body)

	if statusCode != 200 {
		w.WriteHeader(http.StatusBadRequest)
		log.Println(string(body))
		w.Write([]byte("ERROR API SPOTIFY"))
		return
	}

	sb := string(body)
	var refreshToken dtos.ResponseRefreshToken
	json.Unmarshal([]byte(sb), &refreshToken)

	w.Write([]byte(refreshToken.RefreshToken))
}
