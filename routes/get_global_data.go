package routes

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"spawnbig/spotify-listener-backend/dtos"
	"strings"
)

const urlAuth = "https://accounts.spotify.com/api/token"
const urlTop10 = "https://api.spotify.com/v1/playlists/37i9dQZEVXbMDoHDwVN2tF/tracks?limit=10"

func GetGlobalData(w http.ResponseWriter, r *http.Request) {

	accesToken := GetAccessToken()
	client := &http.Client{}
	req, err := http.NewRequest("GET", urlTop10, nil)
	if err != nil {
		log.Println("ERROR AL CREAR OBJETO CLIENTE")
	}
	req.Header.Add("Authorization", "Bearer "+accesToken)
	resp, err := client.Do(req)
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	sb := string(body)

	var top10 dtos.ResponseTop10
	if err := json.Unmarshal([]byte(sb), &top10); err != nil {
		panic(err)
	}
	top10.Name = "TOP 10 MUNDIAL"
	response, err := json.Marshal(top10)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Write(response)
}

func GetAccessToken() string {
	refreshToken := os.Getenv("REFRESH_TOKEN")
	form := url.Values{}
	form.Add("grant_type", "refresh_token")
	form.Add("refresh_token", refreshToken)

	client := &http.Client{}
	req, err := http.NewRequest("POST", urlAuth, strings.NewReader(form.Encode()))
	if err != nil {
		log.Println("ERROR AL CREAR OBJETO CLIENTE")
	}
	auth := fmt.Sprintf("%s:%s", os.Getenv("CLIENT_ID"), os.Getenv("CLIENT_SECRET"))
	base64Auth := base64.StdEncoding.EncodeToString([]byte(auth))
	basicAuth := fmt.Sprintf("%s %s", "Basic", base64Auth)

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Authorization", basicAuth)
	resp, err := client.Do(req)
	if err != nil {
		log.Println("ERROR AL REALIZAR PETICION")
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	sb := string(body)

	var resTop10 dtos.ResponseAccessToken
	json.Unmarshal([]byte(sb), &resTop10)
	return resTop10.AccesToken
}
